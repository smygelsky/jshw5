function createNewUser() {

    fName = prompt('Enter first name');

    lName = prompt('Enter last name');

    bDay= prompt('Enter your birth date\n(please enter it in "dd.mm.yyyy" format)',"dd.mm.yyyy");

    bDayArr = bDay.split(".");


    newUser = {
        _firstName: fName,
        _lastName: lName,
        birthday: new Date(bDayArr[2],bDayArr[1]-1,bDayArr[0]),

        setFirstName(newValue){
            if (newValue==="" || newValue=== null)
                this._firstName = prompt('Enter first name');
            else
                this._firstName = newValue;
        },

        setLastName(newValue){
            if (newValue==="" || newValue=== null)
                this._lastName = prompt('Enter last name');
            else
                this._lastName = newValue;
        },

        getLogin: function(){
            return (this._firstName.substring(0,1) + this._lastName).toLowerCase();
        },

        getAge: function(){
            let dt = new Date();
            return dt.getFullYear() - this.birthday.getFullYear();
        },

        getPassword: function(){
            return this._firstName.substring(0,1).toUpperCase() + this._lastName.toLowerCase()+this.birthday.getFullYear();
        },

        get FirstName(){
            return this._firstName;
        },

        get LastName(){
            return this._lastName;
        }

    };
    return newUser
}

let user =  createNewUser();


    console.log("Login: "+user.getLogin());
    console.log("========================");
    console.log("Age: "+user.getAge());
    console.log("========================");
    console.log("Password :"+user.getPassword());
    console.log("========================");


    //Экранирование символов — замена в тексте управляющих символов на соответствующие текстовые подстановки.
    //Необходимо нампример для вывода текста с определыми знаками, так чтоб символы не являлись управляющими для языка.